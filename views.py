
from pickle import FALSE
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from django.contrib.auth import login, logout, authenticate
from rest_framework import viewsets, status
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.utils import timezone
import datetime
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.decorators import action
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import IsAuthenticated
from .models import Session, deEmojify
from .models import User
from .models import Option
from .models import Header
from .models import Contact
from .models import UserChat
from .models import Chat as Conversacion
from .models import Message
from django.db.models import Count,Sum
from django.views.decorators.clickjacking import xframe_options_deny

from django.db import transaction

from .serializers import UserSerializer
from .serializers import UserModelSerializer
from .serializers import UserLoginSerializer
from .serializers import OptionSerializer
from .serializers import HeaderSerializer
from .serializers import ContactSerializer
import environ
env = environ.Env()
# Create your views here.

class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening

class check_login(APIView):
    #permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        #from .tasks import waitNSeconds
        #waitNSeconds.delay(3)
        return Response("ok")

class Dashboard_APIView(APIView):
    def get(self,request):
        result={}
        result['usuarios_atendidos']=Conversacion.objects.values('user').annotate(total=Count('user')).count()
        result['chat']=Conversacion.objects.all().count()
        result['respuestas']=Conversacion.objects.aggregate(total=Sum('given_response'))['total']
        #print(result)
        result['sesiones']=Conversacion.objects.values('session_status').filter(session_status__gte=2).order_by('session_status').annotate(total=Count('session_status'))
        #listar el primer nivel de menus
        menu_principal=Option.objects.filter(parent__isnull=True).first()
        result['top_opciones']=OptionSerializer(Option.objects.filter(parent=menu_principal,view__gte=1).order_by('view')[:5], many=True).data;
        #five_minutes_ago = timezone.now() + datetime.timedelta(minutes=-5)
        #result['usuarios_activos']= Conversacion.objects.filter(modified__gte=five_minutes_ago).count()
        result['usuarios_activos']= Session.objects.filter(active=True).count()
        return Response(result, status = status.HTTP_200_OK)

class User_List_APIView(APIView):
    permission_classes = (IsAuthenticated,)
    #list
    def get(self, request):
        users= User.objects.all()
        serializer= UserSerializer(users,many=True)
        return Response(serializer.data)
    
    # create
    def post(self, request):
        user_serializer = UserSerializer(data = request.data)
        
        #validation
        if user_serializer.is_valid():
            user_serializer.save()
            return Response({'message':'Objeto creado correctamente...'}, status = status.HTTP_201_CREATED)
        
        return Response(user_serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class User_Detail_APIView(APIView):
    permission_classes = (IsAuthenticated,)
    # retrieve
    def get(self, request, pk=None):
        
        # queryset
        user = User.objects.filter(id = pk).first()
        
        # validation
        if user:
            user_serializer = UserSerializer(user)
            return Response(user_serializer.data, status = status.HTTP_200_OK)
        
        else:
            return Response({'message':'No se encontró el objeto'}, status = status.HTTP_400_BAD_REQUEST)
    
    # update
    def put(self, request, pk=None):
        
        # queryset
        user = User.objects.filter(id = pk).first()
        
        # validation
        if user:
            user_serializer = UserSerializer(user, data = request.data)
            
            if user_serializer.is_valid():
                user_serializer.save()
                return Response({'message':'Actualizado correctamente...'}, status = status.HTTP_200_OK)
            
            else:
                return Response(user_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
            
        return Response({'message':'El objeto no fue encontrado...'}, status = status.HTTP_400_BAD_REQUEST)
    
    # delete
    def delete(self, request, pk=None):
        
        # queryset
        user = User.objects.filter(id = pk).first()
        
        # validation
        if user:
            user.delete()
            return Response({'message':'El objeto ha sido eliminado correctamente'}, status = status.HTTP_200_OK)
        
        return Response({'message':'El objeto no fue encontrado...'}, status = status.HTTP_400_BAD_REQUEST)

class Option_List_APIView(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def save_children(self,children,parent=None):

        for child in children:
            option_serializer = OptionSerializer(data=child)
            if option_serializer.is_valid():
                parent_=option_serializer.save()
                if(parent):
                    parent_.parent=parent
                    parent_.save()
                if len(child['hijos'])>0:
                    self.save_children(child['hijos'],parent_)
            else:
                raise Exception(option_serializer.errors)
                     
    # list
    def get(self, request):
        
        options = Option.objects.filter(enable=True)
        options_serializer = OptionSerializer(options, many=True)
        return Response([Option.OptionType.toJson(),options_serializer.data], status=status.HTTP_200_OK)

    # create
    def post(self, request):
        #print(request.data)
        option_serializer = OptionSerializer(data=request.data)

        if option_serializer.is_valid():
            Option.objects.all().delete()
            parent=option_serializer.save()
            self.save_children(request.data['hijos'],parent)
            return Response({"message": "Objeto creado correctamente..."},status=status.HTTP_201_CREATED,)

        return Response(option_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Option_Detail_APIView(APIView):
    permission_classes = (IsAuthenticated,)
    #Retrieve
    def get(request, pk=None):
        option = Option.objects.filter(id=pk).first()

        if option:
            option_serializer = OptionSerializer(option)
            return Response(option_serializer.data, status=status.HTTP_200_OK)
        
        return Response({"message": "Objeto no encontrado..."}, status=status.HTTP_400_BAD_REQUEST)
    
    #Update
    def put(request, pk=None):
        option = Option.objects.filter(id=pk).first()

        if option:
            option_serializer = OptionSerializer(option, data=request.data)
            
            if option_serializer.is_valid():
                option_serializer.save()
                return Response({"message": "Actualizado correctamente..."},status=status.HTTP_200_OK)
            else:
                return Response(option_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        return Response({"message": "Objeto no encontrado..."}, status=status.HTTP_400_BAD_REQUEST)
    
    # delete
    def delete(self, request, pk=None):

        # queryset
        option = Option.objects.filter(id=pk).first()

        # validation
        if option:
            option.enable = False
            option.save()
            return Response({"message": "Objeto eliminado correctamente..."},status=status.HTTP_200_OK)

        return Response({"message": "Objeto no encontrado..."}, status=status.HTTP_400_BAD_REQUEST)

class Header_List_APIView(APIView):
    
    #permission_classes = (IsAuthenticated,)
    #list
    def get(self, request):
        headers = Header.objects.all()
        serializer= HeaderSerializer(headers,many=True)
        return Response(serializer.data)
    
    # create
    def post(self, request):
        header_serializer = HeaderSerializer(data = request.data)
        
        #validation
        if header_serializer.is_valid():
            header_serializer.save()
            return Response({'message':'Objeto creado correctamente...'}, status = status.HTTP_201_CREATED)
        
        return Response(header_serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class Header_Detail_APIView(APIView):
    permission_classes = (IsAuthenticated,)
    # retrieve
    def get(self, request, pk=None):
        
        # queryset
        header = Header.objects.filter(id = pk).first()
        
        # validation
        if header:
            header_serializer = HeaderSerializer(header)
            return Response(header_serializer.data, status = status.HTTP_200_OK)
        
        else:
            return Response({'message':'No se encontró el objeto'}, status = status.HTTP_400_BAD_REQUEST)
    
    # update
    def put(self, request, pk=None):
        
        # queryset
        header = Header.objects.filter(id = pk).first()
        
        # validation
        if header:
            header_serializer = HeaderSerializer(header, data = request.data)
            
            if header_serializer.is_valid():
                header_serializer.save()
                return Response({'message':'Actualizado correctamente...'}, status = status.HTTP_200_OK)
            
            else:
                return Response(header_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
            
        return Response({'message':'El objeto no fue encontrado...'}, status = status.HTTP_400_BAD_REQUEST)
    
    # delete
    def delete(self, request, pk=None):
        
        # queryset
        header = Header.objects.filter(id = pk).first()
        
        # validation
        if header:
            header.delete()
            return Response({'message':'El objeto ha sido eliminado correctamente'}, status = status.HTTP_200_OK)
        
        return Response({'message':'El objeto no fue encontrado...'}, status = status.HTTP_400_BAD_REQUEST)

class Contact_List_APIView(APIView):
    
    permission_classes = (IsAuthenticated,)
    #list
    def get(self, request):
        contacts = Contact.objects.all()
        serializer= ContactSerializer(contacts,many=True)
        return Response(serializer.data)
    
    # create
    def post(self, request):
        contact_serializer = ContactSerializer(data = request.data)
        
        #validation
        if contact_serializer.is_valid():
            contact_serializer.save()
            return Response({'message':'Objeto creado correctamente...'}, status = status.HTTP_201_CREATED)
        
        return Response(contact_serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class Contact_Detail_APIView(APIView):
    permission_classes = (IsAuthenticated,)
    # retrieve
    def get(self, request, pk=None):
        
        # queryset
        contact = Contact.objects.filter(id = pk).first()
        
        # validation
        if contact:
            contact_serializer = ContactSerializer(contact)
            return Response(contact_serializer.data, status = status.HTTP_200_OK)
        
        else:
            return Response({'message':'No se encontró el objeto'}, status = status.HTTP_400_BAD_REQUEST)
    
    # update
    def put(self, request, pk=None):
        
        # queryset
        contact = Contact.objects.filter(id = pk).first()
        
        # validation
        if contact:
            contact_serializer = ContactSerializer(contact, data = request.data)
            
            if contact_serializer.is_valid():
                contact_serializer.save()
                return Response({'message':'Actualizado correctamente...'}, status = status.HTTP_200_OK)
            
            else:
                return Response(contact_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
            
        return Response({'message':'El objeto no fue encontrado...'}, status = status.HTTP_400_BAD_REQUEST)
    
    # delete
    def delete(self, request, pk=None):
        
        # queryset
        contact = Contact.objects.filter(id = pk).first()
        
        # validation
        if contact:
            contact.delete()
            return Response({'message':'El objeto ha sido eliminado correctamente'}, status = status.HTTP_200_OK)
        
        return Response({'message':'El objeto no fue encontrado...'}, status = status.HTTP_400_BAD_REQUEST)

def close_Sesion(request):
    logout(request)
    return redirect('Home')

class AuthUser(APIView):

    # RETRIEVE
    def get(self,request):
        data={
            'preguntas':[
                {'pregunta':'Escriba su numero de documento (1223334444)','id':4},
                {'pregunta':'Escriba fecha de expedicion DD-MM-AAAA (01-05-2022)','id':9}
            ]
        }
        return Response(data, status=status.HTTP_200_OK)

    # CREATE
    def post(self,request):
        data={
            'token':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
            'msg':'¡Autenticacion Exitosa!'
        }
        respuestas=request.data['respuestas']
        if respuestas[0]['id']==4 and respuestas[0]['respuesta']=='1223334444' and respuestas[1]['id']==9 and respuestas[1]['respuesta']=='01-05-2022':
            return Response(data, status=status.HTTP_200_OK)
        data['msg']='Autenticacion ¡Fallida!'
        return Response(data, status=status.HTTP_401_UNAUTHORIZED)

class Chat(APIView):

    def render_options(self,usuario,options_serializer):
        counter=1
        text=''
        token=usuario.token_auth
        for opcion in options_serializer.data:
            
            if opcion['type']==4:#preguntas Api
                try:
                    import requests
                    response=requests.get(env.str('URL_BASE_PREGUNTAS')+opcion['title'],headers={"Authorization":token})
                    if response.status_code==200:
                        #print(response.json())
                        usuario.realizando_preguntas_api=[True,response.json(),usuario.parent,env.str('URL_BASE_RESPUESTAS')+opcion['title']]
                        text=text+usuario.preguntas_api+"\n"
                    else:
                        text=text+"Opcion fuera de servicios\n"
                except:
                    text=text+"Opcion fuera de servicios\n"
            else:
                if opcion['type']==3:#respuesta Api
                    try:
                        import requests
                        response=requests.get(opcion['title'],headers={"Authorization":token})
                        if response.status_code==200:
                            text=text+response.text+"\n"
                        else:
                            text=text+"Opcion fuera de servicios\n"
                    except:
                        text=text+"Opcion fuera de servicios\n"
                
                else:
                    if(opcion['type']==2):
                        text=text+opcion['title']+"\n"
                    else:
                        text=text+str(counter)+". "+opcion['title']+"\n"
            counter=counter+1
        return text

    def crear_menu(self,usuario):
        opciones=Option.objects.filter(parent=usuario.parent)
        #verificar si es respuesta
        
        options_serializer = OptionSerializer(opciones, many=True)
        header=Option.objects.get(pk=usuario.parent).header
        result={}
        text=''

        if len(options_serializer.data)==1:
            #print("Actualizando")
            #print(options_serializer.data[0]['type'])
            if(options_serializer.data[0]['type']==2 or options_serializer.data[0]['type']==3):
                #contar como respuesta entregada
                usuario.register_response()
                #print("Actualizando")
            #verificar que sea un link de descarga
            if options_serializer.data[0]['file']:
                result['file']=options_serializer.data[0]
            else:
                text=self.render_options(usuario,options_serializer)
        else:
            text=self.render_options(usuario,options_serializer)
        if usuario.parent>1:
            text=text+"0. Atras\n"
        result['pk_menu']=usuario.parent
        result['data']=text
        
        if header:
            result['header']=HeaderSerializer(header).data
        else:
            result['data']=text

        #set emojis
        
        return result
      
    def post(self,request):
            usuario=UserChat(request.data,request.user,'POST')
            msg=Message()
            try:
                msg.chat=Conversacion.objects.get(pk=usuario.chat)
                msg.data=deEmojify(request.data['respuesta'])
                msg.is_client=False
                msg.save();
            except:
                return HttpResponse({"msg":"Error al guardar mensaje"},status=status.HTTP_400_BAD_REQUEST)
            return HttpResponse({"msg":"mensaje guardado exitosamente"},status=status.HTTP_201_CREATED)
    
    def __opcion_invalida(self):
        try:
            header=Header.objects.get(invalid_option=True)
            return Response({"header":HeaderSerializer(header).data}, status=status.HTTP_200_OK)
        except:
            return Response({"data":"Opcion Invalida"}, status=status.HTTP_200_OK)

    def __header_fuera_servicios(self,usuario):
        #print("Enviando mensaje de fuera de servicios")
        menu=self.crear_menu(usuario)
        try:
            header=Header.objects.get(out_of_service=True)
            menu['header']=HeaderSerializer(header).data
        except:
            menu['header']={"message":"Opcion fuera de servicios"}
        return Response(menu, status=status.HTTP_200_OK)

    def get(self,request):
        #print('PETICIONNNN: ', request.GET)
        if 'opcion' in request.GET and 'id' in request.GET:
            usuario=UserChat(request.GET,request.user)
            msg=Message()
            msg.chat=Conversacion.objects.get(pk=usuario.chat)
            msg.data=deEmojify(request.GET['opcion'])
            msg.save();
            
            #buscar la opcion selecionada
            if not(usuario.iniciando_auth) and not(usuario.realizando_preguntas_api):
                opciones=None
                
                if not(usuario.iniciado):
                    print("Iniciando chat con "+str(usuario.id)+", por medio de "+str(usuario.medio))
                
                else:
                    
                    #revisar si mando alguna opcion valida
                    if 'opcion' in request.GET:
                        try:
                            opcion_escogida= int(request.GET['opcion'])
                        except:
                            opcion_escogida=-1
                        #buscar la opcion escogida segun el orden en que se envia
                        try:
                            if opcion_escogida>0:
                                #print("Escogio una opcion")
                                nuevo_menu=Option.objects.values('pk','required_auth').filter(parent=usuario.parent)[opcion_escogida-1]
                                
                                #preguntar si el nuevo menu requiere autenticacion
                                if nuevo_menu['required_auth'] and not(usuario.autenticado):
                                    #print("Iniciando proceso de autenticacion con "+str(usuario.id)+", por medio de "+str(usuario.medio))
                                    #solicitar preguntas
                                    import requests
                                    result=requests.get(env.str('URL_PREGUNTAS_AUTH'))
                                    #print(result)
                                    if result.status_code==200:
                                        usuario.next_menu_auth=nuevo_menu['pk']
                                        usuario.iniciando_auth=True
                                        try:
                                            result_={}
                                            usuario.preguntas_auth=result.json()
                                            #print(env.str('MSG_AUTH'))
                                            result_['data']="\r"+env.str('MSG_AUTH')+"\n\n\r"+usuario.preguntas_auth
                                            return Response(result_, status=status.HTTP_200_OK)
                                        except:
                                            return self.__header_fuera_servicios(usuario)

                                    else:
                                        #api-rest fuera de servicio
                                        return self.__header_fuera_servicios(usuario)
                                #actualizar menu del usuario
                                else:
                                    usuario.parent=nuevo_menu['pk']
                            
                            elif opcion_escogida==0:
                                #ir atras
                                nuevo_menu=Option.objects.filter(pk=usuario.parent).first()
                                
                                if(nuevo_menu.parent):
                                    usuario.parent=nuevo_menu.parent.pk
                            
                            else:
                                return self.__opcion_invalida()
                                    
                        except:
                            return self.__opcion_invalida()
                    else:
                        return self.__opcion_invalida()
                
            
                
            
                return Response(self.crear_menu(usuario), status=status.HTTP_200_OK)
            
            else:
                if usuario.realizando_preguntas_api:
                    termino=usuario.add_respuestas_api(request.GET['opcion'])
                    if termino:
                        import requests
                        msg=""
                        #print(usuario.respuestas_auth)
                        resultado=requests.post(usuario.url_respuesta_api,headers={"Authorization":usuario.token_auth},json=usuario.respuestas_api)
                        usuario.realizando_preguntas_api=[False]
                        usuario.parent=usuario.last_menu_api
                        nuevo_menu=Option.objects.filter(pk=usuario.last_menu_api).first()
                        if(nuevo_menu.parent):
                            usuario.parent=nuevo_menu.parent.pk
                        #print(resultado.text)
                        if(resultado.status_code==200):
                            msg=resultado.text
                            #motrar el siguiente menu
                        
                        elif resultado.status_code==401:
                            try:
                                msg=resultado.text
                            except:
                                msg="❌❌❌*Informacion Invalida*❌❌❌"                            
                        else:
                            msg="❌❌❌*Informacion Invalida*❌❌❌" #.env
                        result={}
                        result['data']=msg+"\n\r0. Atras"
                        return Response(result, status=status.HTTP_200_OK)
                        
                    else:
                        result['data']=usuario.preguntas_api
                    
                    #return Response({"data":"Estoy en modo preguntas auth"}, status=status.HTTP_200_OK)
                else:
                    result={}
                    
                    if 'opcion' in request.GET:
                        termino=usuario.add_respuestas_auth(request.GET['opcion'])
                        
                        if termino:
                            import requests
                            msg=""
                            #print(usuario.respuestas_auth)
                            autenticando=requests.post(env.str('URL_RESPUESTAS_AUTH'),json=usuario.respuestas_auth)
                            if(autenticando.status_code==200):
                                usuario.autenticado=True
                                usuario.parent=usuario.next_menu_auth
                                usuario.token_auth=autenticando.json()['token']
                                try:
                                    msg=autenticando.json()['msg']
                                except:
                                    msg="¡Autenticacion Exitosa!"
                                #motrar el siguiente menu
                            
                            elif autenticando.status_code==401:
                                try:
                                    msg=autenticando.json()['msg']
                                except:
                                    msg="Autenticacion Invalida"
                                usuario.autenticado=False
                                #validacion fallida, mostrar el mismo menu
                            
                            else:
                                msg="Autenticacion Error" #.env
                                usuario.autenticado=False
                            usuario.iniciando_auth=False
                            result=self.crear_menu(usuario)
                            result['data']=msg+"\n\n"+result['data']
                            return Response(result, status=status.HTTP_200_OK)
                        
                        else:
                            result['data']=usuario.preguntas_auth
                    
                    return Response(result, status=status.HTTP_200_OK)
        return Response({"data":"faltan parametros"}, status=status.HTTP_400_BAD_REQUEST)

@xframe_options_deny
def cerrar_sesion(request):
    import requests
    import json
    #ip=request.META['REMOTE_ADDR']
    #print(ip)
    five_minutes_ago = timezone.now() - datetime.timedelta(minutes=2)
    result=[]   
     
    sessiones=Session.objects.filter(modified__lte=five_minutes_ago,active=True)
    
    #print(sessiones)
    for sesion in sessiones:
        #print(sesion)
        user=sesion.user.split('_')[1]
        if user not in result:
            result.append(user)
    sessiones.update(active=False)
    
    #print(result)
    #esta parte debe ser generica deacuerdo a que medio se tiene el chat, por ahora fija
    headers = {'content-type': 'application/json'}
    header = Header.objects.filter(header_type='GOODBYE').first()
    header=HeaderSerializer(header).data    
    requests.post('http://whatsapp:80/broadcast',json={'contactos':result, 'header':header}, headers=headers)
    return HttpResponse(result)

#sercicio de jhonatan
@xframe_options_deny
def notification(request):
    permission_classes = (IsAuthenticated,)
    data=request.GET

    try:
        contacto = []
        contacto.append(data['destnbr'])
        message = data['message']
        validar_token = False
        tokens = User.objects.all().values('auth_token')
        
        for i in tokens:
            if i['auth_token']==data['token']:
                validar_token = True
                break
        
        if validar_token:
            import requests
            headers = {'content-type': 'application/json'}         
            result=requests.post('http://whatsapp:80/broadcast',json={'contactos':contacto, 'data':message}, headers=headers)
            return HttpResponse('Exito')
        else:
            return HttpResponse('El token enviado no existe')
    
    except Exception as e:
        return HttpResponse(str(e))

class BroadcastBridge(APIView):
    permission_classes = (IsAuthenticated,)
    
    # create
    def post(self, request):
        
        try:
            contactos = []
            import requests

            if request.data['select'] == 'contactos_con_chat':
                contactos_con_chat = Conversacion.objects.values('user').order_by('user').distinct()
                for contacto in contactos_con_chat:
                    contactos.append(contacto['user'])
                
            elif request.data['select'] == 'todos_los_contactos':
                contactos = request.data['contactos']
                contactos_con_chat = Conversacion.objects.values('user').order_by('user').distinct()
                
                for contacto in contactos_con_chat:
                    if not contacto['user'] in contactos:
                        contactos.append(contacto['user'])
                
            else:
                contactos = request.data['contactos']
    
            headers = {'content-type': 'application/json'}
            header = Header.objects.get(pk=request.data['header'])
            header=HeaderSerializer(header).data            
            result=requests.post('http://whatsapp:80/broadcast',json={'contactos':contactos, 'header':header}, headers=headers)
            #print(result.text)
            return Response({'message':'Objeto mandado correctamente...'}, status=result.status_code)
        
        except Exception as e:
            return Response(str(e))

class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening